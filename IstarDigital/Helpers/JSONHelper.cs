﻿using System.Web.Script.Serialization;
using System.Data;
using System.Collections.Generic;
using System;
using Newtonsoft.Json.Converters;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IstarDigital.Helpers
{
    public static class JSONHelper
    {
        #region Public extension methods.
        /// <summary>
        /// Extened method of object class
        /// Converts an object to a json string.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJSON(this object obj)
        {
            var serializer = new JavaScriptSerializer();
            try
            {
                return serializer.Serialize(obj);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string ToJSONv2s(this object obj)
        {

            try
            {
                string temp = JsonConvert.SerializeObject(obj);

                JObject rss = JObject.Parse(temp);

                return rss.ToString(Formatting.None);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        //public static string ToJSONDT(this object obj)
        //{

        //    try
        //    {
        //        string temp = obj.ToString();
        //        MatchEvaluator matchEvaluator = ConvertJsonDateToDateString;
        //        var reg = new Regex(@"\\/Date\(\d+\)\\/");
        //        temp = reg.Replace(temp, matchEvaluator);
        //        return temp;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

        //public static string ConvertJsonDateToDateString(Match m)
        //{
        //    var dt = new DateTime(1970, 1, 1);
        //    dt = dt.AddMilliseconds(long.Parse(m.ToString()));
        //    dt = dt.ToLocalTime();
        //    var result = dt.ToString("yyyy-MM-dd HH:mm:ss");
        //    return "tes";
        //}

        #endregion
    }
}