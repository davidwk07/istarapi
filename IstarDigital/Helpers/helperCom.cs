﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IstarDigital.Models;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Security.Claims;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security;

namespace IstarDigital.Helpers
{
    public class helperCom
    {
        public bool sendEmailViaWebApi(emailParam mailParam)
        {
            bool result = false;

            try
            {

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(mailParam.SMTPServer);
                mail.From = new MailAddress(mailParam.fromMail);
                mail.To.Add(mailParam.sendemail);
                mail.Subject = mailParam.subjectMsg;
                mail.Body = mailParam.bodyMsg;
                mail.IsBodyHtml = mailParam.SMTPbodyHTML;
                SmtpServer.Port = mailParam.PortServer;
                SmtpServer.Credentials = new System.Net.NetworkCredential(mailParam.SMTPId, mailParam.SMTPPassword);
                SmtpServer.EnableSsl = mailParam.SMTPSSL;
                SmtpServer.Send(mail);

            }
            catch(Exception ex)
            {
                //throw ex;
                helperLib.writeLogBLL("sendEmailViaWebApi", "Function", "", "400", ex.Message.ToString());
                result = true;
            }

            helperLib.writeLogMedian("EMAIL", mailParam.sendemail, mailParam.bodyMsg, "200", "");

            return result;
        }

        public string RandomString(int Size)
        {
            var random = new Random();

            string input = "abcdefghijkmnopqrstuvwyxz0123456789ABCDEFGHJKLMNOPQRSTUVWXYZ";
            var chars = Enumerable.Range(0, Size)
                                   .Select(x => input[random.Next(0, input.Length)]);
            return new string(chars.ToArray());
        }

        public string RandomStringNumber(int Size)
        {
            var random = new Random();

            string input = "0123456789";
            var chars = Enumerable.Range(0, Size)
                                   .Select(x => input[random.Next(0, input.Length)]);
            return new string(chars.ToArray());
        }

        public string isms_SendSMS(smsParam SMSParam)
        {

            HttpWebRequest myHttpWebRequest = default(HttpWebRequest);
            //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebResponse myHttpWebResponse = default(HttpWebResponse);
            //Declare an HTTP-specific implementation of the WebResponse class
            string sresponse = "";
            bool result = false;

            try
            {
                string inURL = SMSParam.serviceLink + "userid=" + SMSParam.userID + "&password=" + SMSParam.userPassword + "&msisdn=" + SMSParam.phoneNo + "&message=" + SMSParam.SMSContent.Replace("&", " ") + "&sender=" + SMSParam.senderID + "&division=" + SMSParam.divisionID + "&batchname=" + SMSParam.batchname + "&uploadby=" + SMSParam.uploadBy + "&Channel=" + SMSParam.channel;

                //Create Request
                myHttpWebRequest = ((HttpWebRequest)HttpWebRequest.Create(inURL));
                myHttpWebRequest.Credentials = CredentialCache.DefaultCredentials;
                myHttpWebRequest.Method = "GET";
                myHttpWebRequest.ContentType = "text/html; charset=utf-8";

                //Get Response
                myHttpWebResponse = ((HttpWebResponse)myHttpWebRequest.GetResponse());

                WebResponse myresponse = ((HttpWebResponse)myHttpWebRequest.GetResponse());
                Stream receiveStream = myresponse.GetResponseStream();

                // Pipes the stream to a higher level stream reader with the required encoding format.  
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                sresponse = readStream.ReadToEnd();

                // Clean up the streams.
                receiveStream.Close();
                readStream.Close();
                myresponse.Close();

                //return sresponse;
            }
            catch(Exception ex)
            {
                helperLib.writeLogBLL("isms_SendSMS", "Function", "", "", ex.Message.ToString());
                sresponse = sresponse+"ERROR";
            }

            helperLib.writeLogMedian("SMS", SMSParam.phoneNo, SMSParam.SMSContent.Replace("&", " "), "200", "");

            return sresponse;
        }

        /*
           updated : RS
           date : 31 MAY 2018
           Reason : ADD Date value Token check for Old Token 
       */

        public static string GenerateLocalAccessTokenResponse(string UserName , string LastKeyUpdated)
        {

            var tokenExpiration = TimeSpan.FromDays(1);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, UserName));
            identity.AddClaim(new Claim(ClaimTypes.Role, "CLIENT_PRTL_USER"));
            identity.AddClaim(new Claim("LastKeyUpdate", LastKeyUpdated));

            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);

            //JObject tokenResponse = new JObject(
            //                            new JProperty("userName", UserName),
            //                            new JProperty("access_token", accessToken),
            //                            new JProperty("token_type", "bearer"),
            //                            new JProperty("expires_in", tokenExpiration.TotalSeconds.ToString()),
            //                            new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
            //                            new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString())
            //);

            return accessToken;
        }
    }
}