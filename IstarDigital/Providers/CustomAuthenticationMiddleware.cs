﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using IstarDigital.Models;

namespace IstarDigital.Providers
{
    public class CustomAuthenticationMiddleware : OwinMiddleware
    {
        public CustomAuthenticationMiddleware(OwinMiddleware next)
       : base(next)
        {
        }

        public override async Task Invoke(IOwinContext context)
        {
            await Next.Invoke(context);

            if (context.Response.StatusCode == 400
                && context.Response.Headers.ContainsKey(
                          ServerGlobalModels.OwinFlag))
            {
                var headerValues = context.Response.Headers.GetValues
                      (ServerGlobalModels.OwinFlag);

                context.Response.StatusCode =
                       Convert.ToInt16(headerValues.FirstOrDefault());

                context.Response.Headers.Remove(
                       ServerGlobalModels.OwinFlag);
            }

        }
    }
}