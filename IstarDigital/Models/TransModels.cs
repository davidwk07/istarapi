﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IstarDigital.Models
{
    public class AttachmentType
    {
        public string MimeType { get; set; }
        public string FriendlyName { get; set; }
        public string Extension { get; set; }
    }

    public class FileParam
    {
        public string UserID { get; set; }
        public string Path { get; set; }
    }

    public class FileResults
    {
        public string FileString { get; set; }
        //public byte[] FileString { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class adminForm2Param
    {
        public string SearchParam { get; set; }
        public int pageNum { get; set; }
        public int pageSize { get; set; }
    }

    public class adminForm2Result
    {
        public List<adminForm2List> adminFormList { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }

    }

    public class adminForm2List
    {
        public string formType { get; set; }
        public List<adminForm2ListForm> formList { get; set; }

    }

    public class adminForm2ListForm
    {
        public int totalRows { get; set; }
        public int sequenceNo { get; set; }
        public int contentID { get; set; }
        public string contentTitle { get; set; }
        public string documentLink { get; set; }

    }

    #region ReferralProgram

    public class ReferralParam
    {
        public string userId { get; set; }
        public string PolicyNo { get; set; }
        public string PageNo { get; set; }
        public string PageSize { get; set; }
        //public string token { get; set; }
    }
    public class HasActivePolicyResult
    {
        public bool hasActivePolicy { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class RecommendedAgent
    {
        public string agentCode { get; set; }
        public string agentName { get; set; }
        public string agentPhoneNumber { get; set; }

    }

    public class RecommededAgentResult
    {
        public List<RecommendedAgent> agents { get; set; }
        public int TotalRows { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class InputReferralResult
    {
        public int TotalRowInserted { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }
    public class Referral
    {
        public string ReferralName { get; set; }
        public string ReferralPhoneNumber { get; set; }
        public string City { get; set; }
        public string RecommendedAgentCode { get; set; }
        public string AgentName { get; set; }
        public string AgentPhoneNumber { get; set; }
    }
    public class InputReferralParam
    {
        public string userId { get; set; }
        public List<Referral> Referrals { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class StaticPDFDocParam
    {
        public string userId { get; set; }
        public string ContentTitle { get; set; }
    }

    public class StaticPDFDocResult
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class ActiveReferralResult
    {
        public bool hasActiveReferral { get; set; }
        public string TotalFeeReceived { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class ReferralIncomeResult
    {
        public List<ReferralIncome> Referrals { get; set; }

        public int TotalOfRecords { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class MyReferralResult
    {
        public string FileName { get; set; }
        public string ReferralPDF { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class MyReferral
    {
        public string referralDate { get; set; }
        public string referralName { get; set; }
        public string referralPhone { get; set; }
        public string referralCity { get; set; }
        public string recommendedAgent { get; set; }
        public string agentPhone { get; set; }

    }

    public class PaymentHistory
    {
        public string TransactionDate { get; set; }
        public string FeeAmount { get; set; }
    }

    public class ReferralIncome
    {
        public string ReferralPolicyNo { get; set; }
        public string ReferralClientId { get; set; }
        public string ReferralClientName { get; set; }
        public string TotalFeeReceived { get; set; }
        public List<PaymentHistory> PaymentHistories { get; set; }
    }
    #endregion

}