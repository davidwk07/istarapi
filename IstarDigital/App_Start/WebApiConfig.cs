﻿using System;
using System.Collections.Generic;
using System.Linq;
using IstarDigital.Providers;
using Newtonsoft.Json.Serialization;
using System.Web.Http;
using System.Net.Http.Formatting;

namespace IstarDigital
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            config.Filters.Add(new CustomActionFilter());
            config.Filters.Add(new CustomActionModelFilter());
            config.Filters.Add(new CustomExceptionFilter());
        }
    }
}
