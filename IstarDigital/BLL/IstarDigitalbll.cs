﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IstarDigital.Models;
using IstarDigital.DAL;
using System.Data;
using IstarDigital.Helpers;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;

namespace IstarDigital.BLL
{
    public class IstarDigitalbll
    {
        public UserLogin GetLoginValue(string UserID, string Userpassword)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            UserLogin oparamLog = null;
            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "get_login_client_user") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@skey1", SqlDbType.VarChar, UserID) as IDbDataParameter,
                    odal.CreateParameter("@skey2", SqlDbType.VarChar, Userpassword) as IDbDataParameter);

                oparamLog = new UserLogin();

                if (rdr.Read())
                {
                    oparamLog.UserID = rdr["UserID"].ToString();
                    oparamLog.UserPassword = rdr["UserPassword"].ToString();
                    oparamLog.userType = rdr["UserType"].ToString();
                    oparamLog.UserStatus = rdr["UserStatus"].ToString();
                    oparamLog.KeyUpdate = rdr["LastKey"].ToString();
                    oparamLog.CodeStatus = rdr["CodeStatus"].ToString();
                    oparamLog.CodeMessage = rdr["CodeMessage"].ToString();
                }
                else
                {
                    oparamLog = null;
                }
            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("GetLoginValue", "StoredProcedure", "usp_get_records", "get_login_client_user", ex.Message.ToString());

                throw ex;

            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return oparamLog;
        }

        public string GetLastKeyPwd(string UserID)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            string result = null;
            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "get_lastKeyUpdate_client_user") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@skey1", SqlDbType.VarChar, UserID) as IDbDataParameter);

                if (rdr.Read())
                {
                    result = rdr["LastKey"].ToString();
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("GetLastKeyPwd", "StoredProcedure", "usp_get_records", "get_lastKeyUpdate_client_user", ex.Message.ToString());
                throw ex;
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }

            return result;
        }



        public HomePage GetHomePage(HomePageParam oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            HomePage ohome = null;
            BannerImage oimg = null;
            int rowno = 0;
            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "client_get_homepage") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@nkey1", SqlDbType.Int, oParam.Height) as IDbDataParameter,
                    odal.CreateParameter("@nkey2", SqlDbType.Int, oParam.Width) as IDbDataParameter,
                    odal.CreateParameter("@dkey1", SqlDbType.DateTime, oParam.LastUpdatedBannerImage) as IDbDataParameter);

                ohome = new HomePage();
                while (rdr.Read())
                {
                    if (rowno == 0)
                    {
                        ohome.LastUpdatedBannerImage = helperLib.GetDate(rdr["LastUpdatedBannerImage"]);
                        ohome.BannerImages = new List<BannerImage>();
                        rowno = +1;
                    }

                    oimg = new BannerImage();
                    oimg.ImageSequence = (int)rdr["ImageSequence"];
                    oimg.ImageDetail = helperLib.Getbyte(rdr["ImageDetail"]);
                    oimg.ImageURL = rdr["ImageURL"].ToString();

                    if (oimg.ImageDetail != null)
                    {
                        ohome.BannerImages.Add(oimg);
                    }
                }

                ohome.Result = "SUCCESS";
                ohome.Message = "";

                if (ohome == null)
                {
                    ohome = new HomePage();
                    ohome.Result = "FAILURE";
                    ohome.Message = "Tidak ada data";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("GetHomePage", "StoredProcedure", "usp_get_records", "client_get_homepage", ex.Message.ToString());
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return ohome;
        }

        public HomePage_Path GetHomePage_Path(HomePageParam oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            HomePage_Path ohome = null;
            BannerImage_Path oimg = null;
            int rowno = 0;
            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "client_get_homepage_path") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@nkey1", SqlDbType.Int, oParam.Height) as IDbDataParameter,
                    odal.CreateParameter("@nkey2", SqlDbType.Int, oParam.Width) as IDbDataParameter,
                    odal.CreateParameter("@dkey1", SqlDbType.DateTime, oParam.LastUpdatedBannerImage) as IDbDataParameter);

                ohome = new HomePage_Path();
                while (rdr.Read())
                {
                    if (rowno == 0)
                    {
                        ohome.LastUpdatedBannerImage = helperLib.GetDate(rdr["LastUpdatedBannerImage"]);
                        ohome.BannerImages = new List<BannerImage_Path>();
                        rowno = +1;
                    }

                    oimg = new BannerImage_Path();
                    oimg.ImageSequence = (int)rdr["ImageSequence"];
                    oimg.ImageDetail = rdr["ImageLink"].ToString();
                    oimg.ImageURL = rdr["ImageURL"].ToString();

                    if (oimg.ImageDetail != null)
                    {
                        ohome.BannerImages.Add(oimg);
                    }
                }

                ohome.Result = "SUCCESS";
                ohome.Message = "";

                if (ohome == null)
                {
                    ohome = new HomePage_Path();
                    ohome.Result = "FAILURE";
                    ohome.Message = "Tidak ada data";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("GetHomePage_Path", "StoredProcedure", "usp_get_records", "client_get_homepage_path", ex.Message.ToString());
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return ohome;
        }

        /*
           updated : RS
           date : 31 MAY 2018
           Reason : ADD Date value Token check for Old Token 
       */

        public DefaultReturn RegisterUser(RegisterParam oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oret = null;
            helperSec objhelperSec = new helperSec();
            try
            {
                oret = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "02") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, oParam.UserName) as IDbDataParameter,
                odal.CreateParameter("@POL_ID", SqlDbType.VarChar, oParam.PolicyID) as IDbDataParameter,
                odal.CreateParameter("@BirthDate", SqlDbType.DateTime, helperLib.GetDateFormat(oParam.BirthDate, "yyyyMMdd")) as IDbDataParameter,
                odal.CreateParameter("@NewPassword", SqlDbType.VarChar, oParam.UserPassword) as IDbDataParameter,
                odal.CreateParameter("@ActivationType", SqlDbType.VarChar, oParam.ActivationType) as IDbDataParameter,
                odal.CreateParameter("@ActivationInfo", SqlDbType.VarChar, oParam.ActivationInfo) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oret.Result, 100, ParameterDirection.Output) as IDbDataParameter,
                odal.CreateParameter("@result2", SqlDbType.VarChar, oret.OtherValue, 100, ParameterDirection.Output) as IDbDataParameter);

                odal.CommitTransactions();

                if (odal.get_ReturnValue(1) == "RG_CLIENT_01")
                {
                    oret.Result = "FAILURE";
                    //oret.Message = "Data Polis dan Tanggal Lahir anda Salah";
                    oret.Message = "Nomor polis, tanggal lahir dan alamat email/nomor ponsel yang Anda masukkan tidak sesuai.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_02")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID telah terdaftar. Gunakan User ID lainnya.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_03")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Nomor polis, tanggal lahir dan alamat email/nomor ponsel yang Anda masukkan tidak sesuai.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_04")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Anda sudah melakukan registrasi. Silakan gunakan fasilitas Lupa Kata Sandi untuk mengetahui informasi login anda.";
                }
                else
                {
                    ResendActivationCode(oParam.UserName);
                    oret.OtherValue = odal.get_ReturnValue(2);
                    oret.Result = "SUCCESS";
                    oret.Message = "";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("RegisterUser", "StoredProcedure", "usp_user_management_client", "", ex.Message.ToString());
                odal.RollBackTransactions();
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

        public DefaultReturn RegisterUserPhase1(RegisterParamV1 oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oret = null;
            helperSec objhelperSec = new helperSec();
            try
            {
                oret = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "08") as IDbDataParameter,
                odal.CreateParameter("@POL_ID", SqlDbType.VarChar, oParam.PolicyID) as IDbDataParameter,
                odal.CreateParameter("@BirthDate", SqlDbType.DateTime, helperLib.GetDateFormat(oParam.BirthDate, "yyyyMMdd")) as IDbDataParameter,
                odal.CreateParameter("@ActivationType", SqlDbType.VarChar, oParam.ActivationType) as IDbDataParameter,
                odal.CreateParameter("@ActivationInfo", SqlDbType.VarChar, oParam.ActivationInfo) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oret.Result, 100, ParameterDirection.Output) as IDbDataParameter);

                odal.CommitTransactions();

                if (odal.get_ReturnValue(1) == "RG_CLIENT_01")
                {
                    oret.Result = "FAILURE";
                    //oret.Message = "Data Polis dan Tanggal Lahir anda Salah";
                    oret.Message = "Data yang Anda masukkan belum sesuai. Hubungi Pusat Layanan Nasabah di 1500786 untuk bantuan.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_03")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Data yang Anda masukkan belum sesuai. Hubungi Pusat Layanan Nasabah di 1500786 untuk bantuan.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_04")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID Anda sudah terdaftar. Silahkan melakukan Login akun Anda.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_05")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID Anda sudah terdaftar. Silahkan melanjutkan aktivasi akun pada halaman Login.";
                }
                else
                {
                    oret.Result = "SUCCESS";
                    oret.Message = "";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("RegisterUserPhase1", "StoredProcedure", "usp_user_management_client", "", ex.Message.ToString());
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

        /*
           updated : RS
           date : 31 MAY 2018
           Reason : ADD Date value Token check for Old Token 
       */

        public DefaultReturn RegisterUserPhase2(RegisterParam oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oret = null;
            helperSec objhelperSec = new helperSec();
            try
            {
                oret = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "09") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, oParam.UserName) as IDbDataParameter,
                odal.CreateParameter("@POL_ID", SqlDbType.VarChar, oParam.PolicyID) as IDbDataParameter,
                odal.CreateParameter("@BirthDate", SqlDbType.DateTime, helperLib.GetDateFormat(oParam.BirthDate, "yyyyMMdd")) as IDbDataParameter,
                odal.CreateParameter("@NewPassword", SqlDbType.VarChar, oParam.UserPassword) as IDbDataParameter,
                odal.CreateParameter("@ActivationType", SqlDbType.VarChar, oParam.ActivationType) as IDbDataParameter,
                odal.CreateParameter("@ActivationInfo", SqlDbType.VarChar, oParam.ActivationInfo) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oret.Result, 100, ParameterDirection.Output) as IDbDataParameter,
                odal.CreateParameter("@result2", SqlDbType.VarChar, oret.OtherValue, 100, ParameterDirection.Output) as IDbDataParameter);

                odal.CommitTransactions();

                if (odal.get_ReturnValue(1) == "RG_CLIENT_01")
                {
                    oret.Result = "FAILURE";
                    //oret.Message = "Data Polis dan Tanggal Lahir anda Salah";
                    oret.Message = "Nomor polis, tanggal lahir dan alamat email/nomor ponsel yang Anda masukkan tidak sesuai.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_02")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID telah terdaftar. Gunakan User ID lainnya.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_03")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Nomor polis, tanggal lahir dan alamat email/nomor ponsel yang Anda masukkan tidak sesuai.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_04")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID Anda sudah terdaftar. Silahkan melakukan Login akun Anda.";
                }
                else if (odal.get_ReturnValue(1) == "RG_CLIENT_05")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "User ID Anda sudah terdaftar. Silahkan melanjutkan aktivasi akun pada halaman Login.";
                }
                else
                {
                    ResendActivationCode(oParam.UserName);
                    oret.OtherValue = odal.get_ReturnValue(2);
                    oret.Result = "SUCCESS";
                    oret.Message = "User ID Anda telah terdaftar dan kode aktivasi akan dikirimkan ke nomor ponsel/email Anda. Silakan lanjutkan aktivasi.";
                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("RegisterUserPhase2", "StoredProcedure", "usp_user_management_client", "", ex.Message.ToString());
                odal.RollBackTransactions();
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

        public DefaultReturn ValidateUserName(String UserName)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            DefaultReturn oret = null;

            try
            {

                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                    odal.CreateParameter("@table", SqlDbType.VarChar, "client_validate_username") as IDbDataParameter,
                    odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                    odal.CreateParameter("@skey1", SqlDbType.VarChar, UserName) as IDbDataParameter);

                oret = new DefaultReturn();

                if (rdr.Read())
                {
                    oret.Message = "User Name sudah digunakan";
                    oret.Result = "FAILURE";
                }
                else
                {
                    oret.Message = "";
                    oret.Result = "SUCCESS";
                }
            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("ValidateUserName", "StoredProcedure", "usp_get_records", "client_validate_username", ex.Message.ToString());
                throw ex;
            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }

            return oret;
        }

        public DefaultReturn ValidateActivationCode(ActivationParam oparam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oret = null;
            oret = new DefaultReturn();
            try
            {

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "06") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, oparam.UserName) as IDbDataParameter,
                odal.CreateParameter("@skey4", SqlDbType.VarChar, oparam.ActivationCode) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oret.Result, 100, ParameterDirection.Output) as IDbDataParameter);

                if (odal.get_ReturnValue(1) == "AC_CLIENT_01")
                {
                    oret.Result = "FAILURE";
                    oret.Message = "Kode aktivasi Anda tidak sesuai.";
                }
                else
                {
                    oret.Result = "SUCCESS";
                    oret.Message = "Aktivasi akun berhasil.";
                }

                odal.CommitTransactions();

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("ValidateActivationCode", "StoredProcedure", "usp_user_management_client", "", ex.Message.ToString());
                throw ex;
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }

            return oret;
        }

        public DefaultReturn changeUserImage(changeUserImage oParam)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oResult = null;
            try
            {
                oResult = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandText("update tuser set UserPicture=@UserPicture,UserPictureDate=@UserPictureDate where UserID=@UserID and UserType=@UserType", true,
                    odal.CreateParameter("@UserPicture", SqlDbType.VarBinary, helperLib.Cbytefrom64(oParam.UserImage)) as IDbDataParameter,
                    odal.CreateParameter("@UserID", SqlDbType.VarChar, oParam.UserName) as IDbDataParameter,
                    odal.CreateParameter("@UserPictureDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                    odal.CreateParameter("@UserType", SqlDbType.VarChar, "C") as IDbDataParameter);

                odal.CommitTransactions();


                oResult.Message = "";
                oResult.Result = "SUCCESS";

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("changeUserImage", "CommandText", "update tuser set UserPicture", "", ex.Message.ToString());
                oResult.Message = "Input Parameter tidak valid atau ada kesalahan System";
                oResult.Result = "FAILURE";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oResult;
        }
        
        public DefaultReturn ResendActivationCode(String UserName)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            ActivationParam oActv = null;
            DefaultReturn oret = null;
            helperCom objhelperCom = new helperCom();
            helperSec objhelperSec = new helperSec();
            oActv = new ActivationParam();
            oret = new DefaultReturn();
            try
            {

                String aktivasiCode = objhelperCom.RandomStringNumber(6);

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "05") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, UserName) as IDbDataParameter,
                odal.CreateParameter("@skey4", SqlDbType.VarChar, objhelperSec.GetMD5(aktivasiCode + "SunClient2017")) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oActv.ActivationType, 100, ParameterDirection.Output) as IDbDataParameter,
                odal.CreateParameter("@result2", SqlDbType.VarChar, oActv.ActivationInfo, 100, ParameterDirection.Output) as IDbDataParameter);

                oActv.ActivationType = odal.get_ReturnValue(1);
                oActv.ActivationInfo = odal.get_ReturnValue(2);

                odal.CommitTransactions();

                if (oActv.ActivationType.ToUpper() == "E")
                {
                    emailParam oEmail = null;
                    odal = new SQLAccess("PORTAL");
                    rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                       odal.CreateParameter("@table", SqlDbType.VarChar, "get_SMTP_setting_ClientAktivasi") as IDbDataParameter,
                       odal.CreateParameter("@skey1", SqlDbType.VarChar, "SMTP_ACT_CODE") as IDbDataParameter,
                       odal.CreateParameter("@skey2", SqlDbType.VarChar, UserName) as IDbDataParameter,
                       odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                       odal.CreateParameter("@skey10", SqlDbType.VarChar, aktivasiCode) as IDbDataParameter);

                    while (rdr.Read())
                    {

                        oEmail = new emailParam();
                        oEmail.fromMail = rdr["fromMail"].ToString();
                        oEmail.sendemail = oActv.ActivationInfo;
                        oEmail.subjectMsg = rdr["subjectMsg"].ToString();
                        oEmail.bodyMsg = rdr["bodyMSG"].ToString();
                        oEmail.SMTPServer = rdr["SMTPServer"].ToString();
                        oEmail.PortServer = (int)rdr["PortServer"];
                        oEmail.SMTPId = rdr["SMTPId"].ToString();
                        oEmail.SMTPPassword = rdr["SMTPPassword"].ToString();
                        oEmail.SMTPSSL = bool.Parse(rdr["SMTPSSL"].ToString());
                        oEmail.SMTPbodyHTML = bool.Parse(rdr["SMTPbodyHTML"].ToString());
                    }

                    if (objhelperCom.sendEmailViaWebApi(oEmail))
                    {
                        oret.Result = "FAILURE";
                        oret.Message = "Reset Kata Sandi Sukses, tetapi Kata Sandi baru gagal dikirim ke alamat email Anda";

                    }
                    else
                    {
                        oret.Result = "SUCCESS";
                        oret.Message = "Kode aktivasi telah terkirim ke alamat email Anda.";
                    }

                }
                else if (oActv.ActivationType.ToUpper() == "M")
                {
                    smsParam oSMS = null;
                    odal = new SQLAccess("PORTAL");
                    rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                       odal.CreateParameter("@table", SqlDbType.VarChar, "get_SMS_setting_ClientAktivasi") as IDbDataParameter,
                       odal.CreateParameter("@skey1", SqlDbType.VarChar, "SMS_ACT_CODE") as IDbDataParameter,
                       odal.CreateParameter("@skey2", SqlDbType.VarChar, UserName) as IDbDataParameter,
                       odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                       odal.CreateParameter("@skey10", SqlDbType.VarChar, aktivasiCode) as IDbDataParameter);

                    while (rdr.Read())
                    {

                        oSMS = new smsParam();
                        oSMS.serviceLink = rdr["serviceLink"].ToString();
                        oSMS.userID = rdr["userID"].ToString();
                        oSMS.userPassword = rdr["userPassword"].ToString();
                        oSMS.phoneNo = oActv.ActivationInfo;
                        oSMS.SMSContent = rdr["SMSContent"].ToString();
                        oSMS.senderID = rdr["senderID"].ToString();
                        oSMS.divisionID = rdr["divisionID"].ToString();
                        oSMS.batchname = rdr["batchname"].ToString();
                        oSMS.uploadBy = rdr["uploadBy"].ToString();
                        oSMS.channel = rdr["channel"].ToString();

                    }

                    string tempresult = null;
                    tempresult = objhelperCom.isms_SendSMS(oSMS);

                    odal.BeginTransactions();

                    odal.ExecuteCommandText("insert into tclient_sms_result (UserID,SMSType,ResponseText,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy) select @UserID,@SMSType,@ResponseText,@CreatedDate,@CreatedBy,@UpdatedDate,@UpdatedBy", true,
                        odal.CreateParameter("@UserID", SqlDbType.VarChar, UserName) as IDbDataParameter,
                        odal.CreateParameter("@SMSType", SqlDbType.VarChar, "ACTIVATION_CODE") as IDbDataParameter,
                        odal.CreateParameter("@ResponseText", SqlDbType.VarChar, tempresult) as IDbDataParameter,
                        odal.CreateParameter("@CreatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                        odal.CreateParameter("@CreatedBy", SqlDbType.VarChar, "SYS") as IDbDataParameter,
                        odal.CreateParameter("@UpdatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                        odal.CreateParameter("@UpdatedBy", SqlDbType.VarChar, "SYS") as IDbDataParameter);

                    odal.CommitTransactions();

                    if (tempresult.Contains("ERROR"))
                    {
                        oret.Result = "FAILURE";
                        oret.Message = "Reset Kata Sandi Sukses, tetapi Kata Sandi baru gagal dikirim ke nomor ponsel Anda";
                    }
                    else
                    {
                        oret.Result = "SUCCESS";
                        oret.Message = "Kode aktivasi telah terkirim ke nomor ponsel Anda.";
                    }

                }

            }
            catch (Exception ex)
            {
                helperLib.writeLogBLL("ResendActivationCode", "StoredProcedure and CommandText", "", "", ex.Message.ToString());
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";

            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

        /*
            updated : RS
            date : 31 MAY 2018
            Reason : ADD Date value Token check for Old Token 
        */
        public cMenuList GetMenuList(cParams pprm)
        {
            SQLAccess odal = new SQLAccess("core");
            IDataReader rdr;

            cMenuListDetail oMenuListDetail;
            cMenuList oMenu;
            oMenu = new cMenuList();
            oMenu.cMenuListDetail = new List<cMenuListDetail>();


            try
            {
                rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                               odal.CreateParameter("@table", SqlDbType.VarChar, "Menu_List") as IDbDataParameter,
                               odal.CreateParameter("@userid", SqlDbType.VarChar, ComLib.GetText(pprm.userid)) as IDbDataParameter,
                               odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter);
                while (rdr.Read())
                {
                    oMenuListDetail = new cMenuListDetail();

                    oMenuListDetail.CompanyId = rdr["CompanyId"].ToString();
                    oMenuListDetail.ObjectId = rdr["ObjectId"].ToString();
                    oMenuListDetail.ObjectName = rdr["ObjectName"].ToString();
                    oMenuListDetail.ObjectTitle = rdr["ObjectTitle"].ToString();
                    oMenuListDetail.ObjectType = rdr["ObjectType"].ToString();
                    oMenuListDetail.ObjectParent = rdr["ObjectParent"].ToString();
                    oMenuListDetail.ObjectSequence = rdr["ObjectSequence"].ToString();
                    oMenuListDetail.ObjectLink = rdr["ObjectLink"].ToString();
                    oMenuListDetail.ObjectImage = rdr["ObjectImage"].ToString();
                    oMenuListDetail.LinkChild = rdr["LinkChild"].ToString();
                    oMenuListDetail.ObjectShortcut = ComLib.GetBool(rdr["ObjectShortcut"]);
                    oMenu.cMenuListDetail.Add(oMenuListDetail);
                }
            }
            // oTAX.message = "SUCCESS"
            // oTAX.result = ""

            // Dim listMenu As List(Of cMenuListDetail2) = New List(Of cMenuListDetail2)
            // listMenu = New cMenuListDetail
            // listMenu = New cMenuList


            // Child = From menuch In oMenu.cMenuListDetail
            // Where menuch.ObjectParent = menu.ObjectId
            // Order By menuch.ObjectSequence Descending
            // Select
            // menuch.ObjectId,
            // menuch.ObjectName,
            // menuch.ObjectLink

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rdr = null/* TODO Change to default(_) if this is not a reference type */;
                pprm = null/* TODO Change to default(_) if this is not a reference type */;
                odal.CloseAllConnection();
                odal = null/* TODO Change to default(_) if this is not a reference type */;
            }
            // Dim listMenu As List(Of cMenuListDetail2) = New List(Of cMenuListDetail2)()
            // listMenu = (From menu In oMenu.cMenuListDetail
            // Where menu.ObjectType = "G"
            // Select New cMenuListDetail With {
            // .ObjectId = menu.ObjectId,
            // .ObjectName = menu.ObjectName,
            // .ObjectLink = menu.ObjectLink,
            // .Child = From menuch In oMenu.cMenuListDetail
            // Where menuch.ObjectParent = menu.ObjectId
            // Order By menuch.ObjectSequence Descending
            // Select New cMenuListDetail With {
            // .ObjectId = menuch.ObjectId,
            // .ObjectName = menuch.ObjectName,
            // .ObjectLink = menuch.ObjectLink
            // }})

            return oMenu;
        }

        public DefaultReturn ChangePassword(ChangePassword oPwd)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            DefaultReturn oparamLog = null;
            try
            {
                oparamLog = new DefaultReturn();

                odal.BeginTransactions();

                odal.ExecuteCommandSP("usp_user_management_client",
                odal.CreateParameter("@FlagP", SqlDbType.VarChar, "00") as IDbDataParameter,
                odal.CreateParameter("@UserID", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                odal.CreateParameter("@OldPassword", SqlDbType.VarChar, oPwd.OldPassword) as IDbDataParameter,
                odal.CreateParameter("@NewPassword", SqlDbType.VarChar, oPwd.NewPassword) as IDbDataParameter,
                odal.CreateParameter("@result", SqlDbType.VarChar, oPwd.Result, 100, ParameterDirection.Output) as IDbDataParameter,
                odal.CreateParameter("@result2", SqlDbType.VarChar, oparamLog.OtherValue, 100, ParameterDirection.Output) as IDbDataParameter);

                if (odal.get_ReturnValue(1) == "CHG_PWD_01")
                {
                    oparamLog.Result = "FAILURE";
                    oparamLog.Message = "Kata Sandi Lama Tidak Sesuai";
                }
                else if (odal.get_ReturnValue(1) == "CHG_PWD_02")
                {
                    oparamLog.Result = "FAILURE";
                    oparamLog.Message = "Kata Sandi Tidak Dapat Menggunakan 5 Kata Sandi Sebelumnya";
                }
                else
                {
                    oparamLog.Result = "SUCCESS";
                    oparamLog.Message = "Pembaharuan Kata Sandi Berhasil";
                }

                oparamLog.OtherValue = odal.get_ReturnValue(2);

                odal.CommitTransactions();

            }
            catch
            {
                odal.RollBackTransactions();
                oparamLog.Result = "FAILURE";
                oparamLog.Message = "Input Parameter tidak valid atau ada kesalahan System";
            }
            finally
            {
                odal.CloseAllConnection();
                odal = null;
            }
            return oparamLog;
        }

        public DefaultReturn ForgotPassword(ForgotPassword oPwd)
        {
            SQLAccess odal = new SQLAccess("PORTAL");
            IDataReader rdr = null;
            DefaultReturn oret = null;
            helperCom objhelperLib = new helperCom();
            helperSec objhelperSec = new helperSec();
            oret = new DefaultReturn();
            try
            {

                rdr = odal.ExecuteDataReader("usp_user_management_client", CommandType.StoredProcedure,
                    odal.CreateParameter("@FlagP", SqlDbType.VarChar, "07") as IDbDataParameter,
                    odal.CreateParameter("@skey2", SqlDbType.VarChar, oPwd.PolicyID) as IDbDataParameter,
                    odal.CreateParameter("@ParamType", SqlDbType.VarChar, oPwd.SendingType.ToString().ToUpper()) as IDbDataParameter,
                    odal.CreateParameter("@ParamValue", SqlDbType.VarChar, oPwd.SendingInfo) as IDbDataParameter);

                oPwd.Result2 = "0";

                while (rdr.Read())
                {

                    if (rdr["RESULT"].ToString() == "FGT_PWD_02")
                    {
                        oPwd.Result2 = "DATA EXISTS";
                        oPwd.Result = "FAILURE";
                        oPwd.Message = "Nomor polis dan alamat email/nomor ponsel tidak sesuai.";
                    }
                    else
                    {
                        oPwd.UserName = rdr["UserID"].ToString();
                        oPwd.Result2 = "DATA EXISTS";
                        String passwordAgent = objhelperLib.RandomString(8);

                        odal.BeginTransactions();

                        odal.ExecuteCommandSP("usp_user_management_client",
                        odal.CreateParameter("@FlagP", SqlDbType.VarChar, "01") as IDbDataParameter,
                        odal.CreateParameter("@UserID", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                        odal.CreateParameter("@NewPassword", SqlDbType.VarChar, objhelperSec.GetMD5(oPwd.UserName.ToUpper() + passwordAgent)) as IDbDataParameter,
                        odal.CreateParameter("@ParamType", SqlDbType.VarChar, oPwd.SendingType.ToString().ToUpper()) as IDbDataParameter,
                        odal.CreateParameter("@ParamValue", SqlDbType.VarChar, oPwd.SendingInfo) as IDbDataParameter,
                        odal.CreateParameter("@flagResult", SqlDbType.Bit, oPwd.ResultFlag, 1, ParameterDirection.Output) as IDbDataParameter,
                        odal.CreateParameter("@result", SqlDbType.VarChar, oPwd.Result, 100, ParameterDirection.Output) as IDbDataParameter);

                        oPwd.ResultFlag = bool.Parse(odal.get_ReturnValue(1));

                        if (odal.get_ReturnValue(2) == "FGT_PWD_01")
                        {
                            oPwd.Result = "SUCCESS";
                            oPwd.Message = "Kata Sandi baru telah terkirim ke alamat email yang terdaftar";
                        }
                        else if (odal.get_ReturnValue(2) == "FGT_PWD_02")
                        {
                            oPwd.Result = "FAILURE";
                            oPwd.Message = "Nomor polis dan alamat email/nomor ponsel tidak sesuai.";
                        }
                        else if (odal.get_ReturnValue(2) == "FGT_PWD_03")
                        {
                            oPwd.Result = "SUCCESS";
                            oPwd.Message = "Kata Sandi baru telah terkirim ke nomor ponsel yang terdaftar";
                        }
                        else if (odal.get_ReturnValue(2) == "FGT_PWD_04")
                        {
                            oPwd.Result = "FAILURE";
                            oPwd.Message = "Nomor polis dan alamat email/nomor ponsel tidak sesuai.";
                        }

                        odal.CommitTransactions();

                        if (oPwd.SendingType.ToString().ToUpper() == "E" && oPwd.ResultFlag == true)
                        {
                            emailParam oEmail = null;
                            odal = new SQLAccess("PORTAL");
                            rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                               odal.CreateParameter("@table", SqlDbType.VarChar, "get_SMTP_setting_ClientforgotPassword") as IDbDataParameter,
                               odal.CreateParameter("@skey1", SqlDbType.VarChar, "SMTP_FG_PWD") as IDbDataParameter,
                               odal.CreateParameter("@skey2", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                               odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                               odal.CreateParameter("@skey10", SqlDbType.VarChar, passwordAgent) as IDbDataParameter);

                            while (rdr.Read())
                            {

                                oEmail = new emailParam();
                                oEmail.fromMail = rdr["fromMail"].ToString();
                                oEmail.sendemail = oPwd.SendingInfo;
                                oEmail.subjectMsg = rdr["subjectMsg"].ToString();
                                oEmail.bodyMsg = rdr["bodyMSG"].ToString();
                                oEmail.SMTPServer = rdr["SMTPServer"].ToString();
                                oEmail.PortServer = (int)rdr["PortServer"];
                                oEmail.SMTPId = rdr["SMTPId"].ToString();
                                oEmail.SMTPPassword = rdr["SMTPPassword"].ToString();
                                oEmail.SMTPSSL = bool.Parse(rdr["SMTPSSL"].ToString());
                                oEmail.SMTPbodyHTML = bool.Parse(rdr["SMTPbodyHTML"].ToString());
                            }

                            if (objhelperLib.sendEmailViaWebApi(oEmail))
                            {
                                oPwd.Result = "FAILURE";
                                oPwd.Message = "Reset Kata Sandi Sukses, tetapi Kata Sandi baru gagal dikirim ke email anda.";
                            }

                        }
                        else if (oPwd.SendingType.ToString().ToUpper() == "M" && oPwd.ResultFlag == true)
                        {
                            smsParam oSMS = null;
                            odal = new SQLAccess("PORTAL");
                            rdr = odal.ExecuteDataReader("usp_get_records", CommandType.StoredProcedure,
                               odal.CreateParameter("@table", SqlDbType.VarChar, "get_SMS_setting_ClientforgotPassword") as IDbDataParameter,
                               odal.CreateParameter("@skey1", SqlDbType.VarChar, "SMS_FG_PWD") as IDbDataParameter,
                               odal.CreateParameter("@skey2", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                               odal.CreateParameter("@CompanyID", SqlDbType.VarChar, "CP") as IDbDataParameter,
                               odal.CreateParameter("@skey10", SqlDbType.VarChar, passwordAgent) as IDbDataParameter);

                            while (rdr.Read())
                            {

                                oSMS = new smsParam();
                                oSMS.serviceLink = rdr["serviceLink"].ToString();
                                oSMS.userID = rdr["userID"].ToString();
                                oSMS.userPassword = rdr["userPassword"].ToString();
                                oSMS.phoneNo = oPwd.SendingInfo;
                                oSMS.SMSContent = rdr["SMSContent"].ToString();
                                oSMS.senderID = rdr["senderID"].ToString();
                                oSMS.divisionID = rdr["divisionID"].ToString();
                                oSMS.batchname = rdr["batchname"].ToString();
                                oSMS.uploadBy = rdr["uploadBy"].ToString();
                                oSMS.channel = rdr["channel"].ToString();

                            }

                            string tempresult = null;
                            tempresult = objhelperLib.isms_SendSMS(oSMS);

                            odal.BeginTransactions();

                            odal.ExecuteCommandText("insert into tclient_sms_result (UserID,SMSType,ResponseText,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy) select @UserID,@SMSType,@ResponseText,@CreatedDate,@CreatedBy,@UpdatedDate,@UpdatedBy", true,
                                odal.CreateParameter("@UserID", SqlDbType.VarChar, oPwd.UserName) as IDbDataParameter,
                                odal.CreateParameter("@SMSType", SqlDbType.VarChar, "FORGOT_PWD") as IDbDataParameter,
                                odal.CreateParameter("@ResponseText", SqlDbType.VarChar, tempresult) as IDbDataParameter,
                                odal.CreateParameter("@CreatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                                odal.CreateParameter("@CreatedBy", SqlDbType.VarChar, "SYS") as IDbDataParameter,
                                odal.CreateParameter("@UpdatedDate", SqlDbType.DateTime, DateTime.Now) as IDbDataParameter,
                                odal.CreateParameter("@UpdatedBy", SqlDbType.VarChar, "SYS") as IDbDataParameter);

                            odal.CommitTransactions();

                            if (tempresult.Contains("ERROR"))
                            {
                                oPwd.Result = "FAILURE";
                                oPwd.Message = "Reset Kata Sandi Sukses, tetapi Kata Sandi baru gagal dikirim ke nomor ponsel anda.";
                            }

                        }

                    }

                }

                if (oPwd.Result2 == "0")
                {
                    oPwd.Result = "FAILURE";
                    oPwd.Message = "User ID Anda belum terdaftar. Lakukan pendaftaran terlebih dahulu.";
                }

                oret.Result = oPwd.Result;
                oret.Message = oPwd.Message;

            }
            catch (Exception ex)
            {
                oret.Result = "FAILURE";
                oret.Message = "Input Parameter tidak valid atau ada kesalahan System";

            }
            finally
            {
                rdr = null;
                odal.CloseAllConnection();
                odal = null;
            }
            return oret;
        }

    }
}