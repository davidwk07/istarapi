﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.VisualBasic
Public Class SQLAccess

#Region "Variables"
    Private _sconn As String = ""
    Private _isUsingTransaction As Boolean = False
    Private _isSingleConnection As Boolean = True
    Private _txn As SqlTransaction

    Private _ReturnValue() As Object

#Region "Connection Variables"

    '-- This connection variable is used for single/multiple proses WITHOUT transaction --
    '-- For multiple process:
    '--     - to open connection  >> run StartGroupConnection 
    '--     - to close connection >> run EndGroupConnection
    '-- For single process:
    '--     - open and close will be done automatically
    Private WithEvents cnn As SqlConnection = Nothing
    Private Sub cnn_StateChange(ByVal sender As Object, ByVal e As System.Data.StateChangeEventArgs) Handles cnn.StateChange
        If e.CurrentState = ConnectionState.Open Then
            'activate application role
        End If
    End Sub

    '-- This connection variable is used for single/multiple proses WITH transaction --
    '-- to open connection >> run BeginTransaction
    '-- to close connection >> run CommitTransaction/RollbackTransaction
    Private WithEvents cnx As SqlConnection = Nothing
    Private Sub cnx_StateChange(ByVal sender As Object, ByVal e As System.Data.StateChangeEventArgs) Handles cnn.StateChange
        If e.CurrentState = ConnectionState.Open Then
            'activate application role
        End If
    End Sub

    '-- This connection variable is used for only reader process, open and close connection will be performed at reader function --
    Private WithEvents crd As SqlConnection = Nothing
    Private Sub crd_StateChange(ByVal sender As Object, ByVal e As System.Data.StateChangeEventArgs) Handles crd.StateChange
        If e.CurrentState = ConnectionState.Open Then
            'activate application role
        End If
    End Sub

#End Region

#End Region

#Region "Properties"
    Public Property Txn() As IDbTransaction
        Get
            Return _txn
        End Get
        Set(ByVal value As IDbTransaction)
            _txn = CType(value, SqlTransaction)
        End Set
    End Property
    Public Property IsSingleConnection() As Boolean
        Get
            Return _isSingleConnection
        End Get
        Set(ByVal value As Boolean)
            _isSingleConnection = value
        End Set
    End Property
    Public Property IsUsingTransaction() As Boolean
        Get
            Return _isUsingTransaction
        End Get
        Set(ByVal value As Boolean)
            _isUsingTransaction = value
        End Set
    End Property
    Public Property sconn() As String
        Get
            Return _sconn
        End Get
        Set(ByVal value As String)
            _sconn = value
        End Set
    End Property
    Public ReadOnly Property ReturnValue(ByVal pIndex As Integer) As String
        Get
            Return _ReturnValue(pIndex)
        End Get
    End Property
#End Region

#Region "Constructor"
    Public Sub New(ByVal psconn As String)
        sconn = psconn
    End Sub
    Public Sub New()
        sconn = "portalcs"
    End Sub
#End Region

#Region "ADO.NET Objects"

#Region "Connections"

    Public Function GetConnectionString() As String
        Return csstring(sconn)
    End Function

    Public Sub StartGroupConnection()
        Try
            cnn = New SqlConnection(GetConnectionString)
            cnn.Open()
            IsSingleConnection = False
            IsUsingTransaction = False
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub EndGroupConnection()
        Try
            If Not cnn Is Nothing Then
                cnn.Close()
                cnn.Dispose()
            End If
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub CloseAllConnection()
        Try
            If Not crd Is Nothing Then
                crd.Close()
                crd.Dispose()
            End If
            If Not cnx Is Nothing Then
                cnx.Close()
                cnx.Dispose()
            End If
            If Not cnn Is Nothing Then
                cnn.Close()
                cnn.Dispose()
            End If
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function csstring(ByVal cstype As String) As String
        Dim m_file_name As String
        Dim m_stream_reader As StreamReader
        Dim cnnstring As String = ""
        Dim arrstring() As String
        Dim bout As Boolean = False
        m_file_name = "ccs.ini"
        Try
            If File.Exists(AppDomain.CurrentDomain.BaseDirectory & m_file_name) Then
                m_stream_reader = New StreamReader(AppDomain.CurrentDomain.BaseDirectory & m_file_name)
                While Not bout
                    cnnstring = m_stream_reader.ReadLine()
                    arrstring = Split(cnnstring, "|")
                    If arrstring(0).ToLower = cstype.ToLower Then
                        cnnstring = arrstring(1)
                        bout = True
                    ElseIf cnnstring Is Nothing Then
                        bout = True
                    End If
                    'If Encryption64.Decrypt(arrstring(0), "#kl$^@<z") = cstype Then
                    '    cnnstring = Encryption64.Decrypt(arrstring(1), "#kl$^@<z")
                    '    bout = True
                    'ElseIf cnnstring Is Nothing Then
                    '    bout = True
                    'End If
                End While
                m_stream_reader.Close()
            End If
        Catch ex As Exception
            Throw ex
        Finally
            m_stream_reader = Nothing
        End Try
        Return cnnstring
    End Function


#End Region

#Region "Transactions"
    Public Sub BeginTransactions()
        Try
            cnx = New SqlConnection(GetConnectionString)
            cnx.Open()
            Txn = cnx.BeginTransaction
            IsSingleConnection = False
            IsUsingTransaction = True
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub CommitTransactions()
        Try
            Txn.Commit()
            If Not cnx Is Nothing Then cnx.Close()
            Txn = Nothing
            IsSingleConnection = True
            IsUsingTransaction = False
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub RollBackTransactions()
        Try
            If Not Txn Is Nothing Then Txn.Rollback()
            If Not cnx Is Nothing Then cnx.Close()
            Txn = Nothing
            IsSingleConnection = True
            IsUsingTransaction = False
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Commands"

    Public Function CreateSelectCommand(ByVal cmdType As CommandType, ByVal cmdText As String, ByVal ParamArray cmdParams As IDbDataParameter()) As IDbCommand
        Dim _SQLSelectCmd As SqlCommand
        Try
            _SQLSelectCmd = New SqlCommand(cmdText)
            _SQLSelectCmd.CommandType = cmdType
            For index As Integer = 0 To cmdParams.Length - 1
                _SQLSelectCmd.Parameters.Add(cmdParams(index))
            Next
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
        Return _SQLSelectCmd
    End Function

    Public Function CreateInsertCommand(ByVal cmdType As CommandType, ByVal cmdText As String, ByVal ParamArray cmdParams As IDbDataParameter()) As IDbCommand
        Dim _SQLInsertCmd As SqlCommand
        Try
            _SQLInsertCmd = New SqlCommand(cmdText)
            _SQLInsertCmd.CommandType = cmdType
            For index As Integer = 0 To cmdParams.Length - 1
                _SQLInsertCmd.Parameters.Add(cmdParams(index))
            Next
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
        Return _SQLInsertCmd
    End Function

    Public Function CreateUpdateCommand(ByVal cmdType As CommandType, ByVal cmdText As String, ByVal ParamArray cmdParams As IDbDataParameter()) As IDbCommand
        Dim _SQLUpdateCmd As SqlCommand
        Try
            _SQLUpdateCmd = New SqlCommand(cmdText)
            _SQLUpdateCmd.CommandType = cmdType
            For index As Integer = 0 To cmdParams.Length - 1
                _SQLUpdateCmd.Parameters.Add(cmdParams(index))
            Next
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
        Return _SQLUpdateCmd
    End Function

    Public Function CreateDeleteCommand(ByVal cmdType As CommandType, ByVal cmdText As String, ByVal ParamArray cmdParams As IDbDataParameter()) As IDbCommand
        Dim _SQLDeleteCmd As SqlCommand
        Try
            _SQLDeleteCmd = New SqlCommand(cmdText)
            _SQLDeleteCmd.CommandType = cmdType
            For index As Integer = 0 To cmdParams.Length - 1
                _SQLDeleteCmd.Parameters.Add(cmdParams(index))
            Next
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
        Return _SQLDeleteCmd
    End Function

#End Region

#Region "Parameters"

    Public Function CreateParameter(ByVal paramName As String, ByVal paramType As SqlDbType, ByVal paramValue As Object) As Object
        Dim param As New System.Data.SqlClient.SqlParameter(paramName, paramType)

        If Not IsDBNull(paramValue) Then
            Select Case paramType
                Case SqlDbType.VarChar, SqlDbType.NVarChar, SqlDbType.Char, SqlDbType.NChar, SqlDbType.Text
                    paramValue = CheckParamValue(CType(paramValue, String))
                Case SqlDbType.DateTime
                    paramValue = CheckParamValue(CType(paramValue, DateTime))
                Case SqlDbType.Bit
                    paramValue = CheckParamValue(CType(paramValue, Boolean))
                Case SqlDbType.Int
                    paramValue = CheckParamValue(CType(paramValue, Integer))
                Case SqlDbType.Float
                    paramValue = CheckParamValue(CType(paramValue, Double))
                Case SqlDbType.Decimal
                    paramValue = CheckParamValue(CType(paramValue, Decimal))

            End Select
        End If
        param.Value = paramValue
        Return param
    End Function

    Public Function CreateParameter(ByVal paramName As String, ByVal paramType As SqlDbType, ByVal direction As ParameterDirection) As Object
        Dim returnVal As System.Data.SqlClient.SqlParameter = CreateParameter(paramName, paramType, DBNull.Value)
        returnVal.Direction = direction
        Return returnVal
    End Function

    Public Function CreateParameter(ByVal paramName As String, ByVal paramType As SqlDbType, ByVal paramValue As Object, ByVal size As Integer) As Object
        Dim returnVal As System.Data.SqlClient.SqlParameter = CreateParameter(paramName, paramType, paramValue)
        returnVal.Size = size
        Return returnVal
    End Function

    Public Function CreateParameter(ByVal paramName As String, ByVal paramType As SqlDbType, ByVal paramValue As Object, ByVal size As Integer, ByVal direction As ParameterDirection) As Object
        Dim returnVal As System.Data.SqlClient.SqlParameter = CreateParameter(paramName, paramType, paramValue)
        returnVal.Size = size
        returnVal.Direction = direction
        Return returnVal
    End Function

    Public Function CreateParameter(ByVal paramName As String, ByVal paramType As SqlDbType, ByVal paramValue As Object, ByVal size As Integer, ByVal precision As Byte) As Object
        Dim returnVal As System.Data.SqlClient.SqlParameter = CreateParameter(paramName, paramType, paramValue)
        returnVal.Size = size
        returnVal.Precision = precision
        Return returnVal
    End Function

    Public Function CreateParameter(ByVal paramName As String, ByVal paramType As SqlDbType, ByVal paramValue As Object, ByVal size As Integer, ByVal precision As Byte, ByVal direction As ParameterDirection) As Object
        Dim returnVal As System.Data.SqlClient.SqlParameter = CreateParameter(paramName, paramType, paramValue)
        returnVal.Size = size
        returnVal.Precision = precision
        returnVal.Direction = direction
        Return returnVal
    End Function

    Public Function CreateParameter(ByVal paramName As String, ByVal sourcecolumn As String, ByVal paramType As SqlDbType) As Object
        Dim returnVal As System.Data.SqlClient.SqlParameter = CreateParameter(paramName, paramType, DBNull.Value)
        returnVal.SourceColumn = sourcecolumn
        Return returnVal
    End Function

    Public Function CreateParameter(ByVal paramName As String, ByVal sourcecolumn As String, ByVal paramType As SqlDbType, ByVal sourceversion As DataRowVersion) As Object
        Dim returnVal As System.Data.SqlClient.SqlParameter = CreateParameter(paramName, paramType, DBNull.Value)
        returnVal.SourceColumn = sourcecolumn
        returnVal.SourceVersion = sourceversion
        Return returnVal
    End Function

    Public Function CreateParameter(ByVal paramName As String, ByVal paramType As SqlDbType, ByVal paramValue As Object, ByVal sourcecolumn As String) As Object
        Dim returnVal As System.Data.SqlClient.SqlParameter = CreateParameter(paramName, paramType, paramValue)
        returnVal.SourceColumn = sourcecolumn
        Return returnVal
    End Function

    Protected Function CheckParamValue(ByVal paramValue As String) As Object
        If String.IsNullOrEmpty(paramValue) Then
            Return ""
        Else
            Return paramValue
        End If
    End Function

    'Protected Function CheckParamValue(ByVal paramValue As DateTime) As Object
    '    If paramValue.Equals(Common.NullDateTime) Then
    '        Return DBNull.Value
    '    Else
    '        Return paramValue
    '    End If
    'End Function

    'Protected Function CheckParamValue(ByVal paramValue As Double) As Object
    '    If paramValue.Equals(Common.NullDouble) Then
    '        Return DBNull.Value
    '    Else
    '        Return paramValue
    '    End If
    'End Function

    'Protected Function CheckParamValue(ByVal paramValue As Decimal) As Object
    '    If paramValue.Equals(Common.NullDecimal) Then
    '        Return DBNull.Value
    '    Else
    '        Return paramValue
    '    End If
    'End Function

    'Protected Function CheckParamValue(ByVal paramValue As Integer) As Object
    '    If paramValue.Equals(Common.NullInt) Then
    '        Return DBNull.Value
    '    Else
    '        Return paramValue
    '    End If
    'End Function

    'Protected Function CheckParamValue(ByVal paramValue As Boolean) As Object
    '    If paramValue.Equals(Common.NullBool) Then
    '        Return Common.NullBool
    '    Else
    '        Return paramValue
    '    End If
    'End Function
#End Region

#End Region

#Region "Methods"

#Region "UpdateDataTable"
    Public Function UpdateDataTable(ByVal pdt As DataTable, ByVal pinsertcmd As SqlCommand, ByVal pupdatecmd As SqlCommand, ByVal pdeletecmd As SqlCommand) As Boolean

        Dim oda As New SqlDataAdapter
        Try
            'set the connection
            If IsUsingTransaction Then
                If Not IsNothing(pinsertcmd) Then
                    pinsertcmd.Connection = Txn.Connection
                    pinsertcmd.Transaction = Txn
                    pinsertcmd.CommandTimeout = 300
                End If
                If Not IsNothing(pupdatecmd) Then
                    pupdatecmd.Connection = Txn.Connection
                    pupdatecmd.Transaction = Txn
                    pupdatecmd.CommandTimeout = 300
                End If
                If Not IsNothing(pdeletecmd) Then
                    pdeletecmd.Connection = Txn.Connection
                    pdeletecmd.Transaction = Txn
                    pdeletecmd.CommandTimeout = 300
                End If
            Else
                If IsSingleConnection Then
                    cnn = New SqlConnection(GetConnectionString)
                    cnn.Open()
                End If
                If Not IsNothing(pinsertcmd) Then pinsertcmd.Connection = cnn
                If Not IsNothing(pupdatecmd) Then pupdatecmd.Connection = cnn
                If Not IsNothing(pdeletecmd) Then pdeletecmd.Connection = cnn
            End If

            'prepare dataadapter
            oda.InsertCommand = pinsertcmd
            oda.UpdateCommand = pupdatecmd
            oda.DeleteCommand = pdeletecmd

            'update datatable
            oda.Update(pdt)

            Return True
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        Finally
            If IsSingleConnection And Not cnn Is Nothing Then cnn.Close()
            If Not pinsertcmd Is Nothing Then pinsertcmd.Dispose()
            If Not pupdatecmd Is Nothing Then pupdatecmd.Dispose()
            If Not pdeletecmd Is Nothing Then pdeletecmd.Dispose()
            If Not oda Is Nothing Then oda.Dispose()
        End Try

    End Function
#End Region

#Region "FillDataTable Methods"
    'This version is used where datatable is parsed as parameters
    Public Function FillDataTable(ByRef pdt As DataTable, ByVal pcleardt As Boolean, ByVal pcmdtype As CommandType, ByVal pcmdtext As String, ByVal ParamArray pcmdParams As IDbDataParameter()) As Boolean
        Dim oda As New SqlDataAdapter
        Dim cmd As SqlCommand = Nothing

        Try

            'clear data table when neccessary
            If pcleardt Then pdt.Clear()

            'prepare command
            cmd = New SqlCommand
            cmd.CommandType = pcmdtype
            cmd.CommandText = pcmdtext
            cmd.CommandTimeout = 300

            If Not pcmdParams Is Nothing Then
                For index As Integer = 0 To pcmdParams.Length - 1
                    cmd.Parameters.Add(pcmdParams(index))
                Next
            End If

            'prepare dataadapter
            oda.SelectCommand = cmd

            'set the connection
            If IsUsingTransaction Then
                cmd.Connection = Txn.Connection
                cmd.Transaction = Txn
            Else
                If IsSingleConnection Then
                    cnn = New SqlConnection(GetConnectionString)
                    cnn.Open()
                End If
                cmd.Connection = cnn
            End If

            'Fill datatable
            oda.Fill(pdt)

            Return True
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        Finally
            If IsSingleConnection And Not cnn Is Nothing Then cnn.Close()
            If Not oda Is Nothing Then oda.Dispose()
            If Not cmd Is Nothing Then cmd.Dispose()
        End Try

    End Function
    'This version will return the datatable
    Public Function FillDataTable(ByVal pdtname As String, ByVal pcmdtype As CommandType, ByVal pcmdtext As String, ByVal ParamArray pcmdParams As IDbDataParameter()) As DataTable
        Dim oda As New SqlDataAdapter
        Dim cmd As SqlCommand = Nothing
        Dim dt As New DataTable(pdtname)

        Try

            'prepare command
            cmd = New SqlCommand
            cmd.CommandType = pcmdtype
            cmd.CommandText = pcmdtext
            cmd.CommandTimeout = 300

            If Not pcmdParams Is Nothing Then
                For index As Integer = 0 To pcmdParams.Length - 1
                    cmd.Parameters.Add(pcmdParams(index))
                Next
            End If

            'prepare dataadapter
            oda.SelectCommand = cmd

            'set the connection
            If IsUsingTransaction Then
                cmd.Connection = Txn.Connection
                cmd.Transaction = Txn
            Else
                If IsSingleConnection Then
                    cnn = New SqlConnection(GetConnectionString)
                    cnn.Open()
                End If
                cmd.Connection = cnn
            End If

            'Fill datatable
            oda.Fill(dt)

            'return datatable
            Return dt

        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        Finally
            If IsSingleConnection And Not cnn Is Nothing Then cnn.Close()
            If Not oda Is Nothing Then oda.Dispose()
            If Not cmd Is Nothing Then cmd.Dispose()
        End Try
    End Function
#End Region

#Region "ExecuteNonQuery Methods"
    Public Function ExecuteCommandSP(ByVal procName As String, ByVal ParamArray procParams As IDbDataParameter()) As Boolean
        Dim cmd As SqlCommand = Nothing
        Try

            'prepare command
            cmd = New SqlCommand(procName)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 300
            If Not procParams Is Nothing Then
                For index As Integer = 0 To procParams.Length - 1
                    cmd.Parameters.Add(procParams(index))
                Next
            End If

            'set the connection
            If IsUsingTransaction Then
                cmd.Connection = Txn.Connection
                cmd.Transaction = Txn
            Else
                If IsSingleConnection Then
                    cnn = New SqlConnection(GetConnectionString)
                    cnn.Open()
                End If

                cmd.Connection = cnn
            End If

            'execute command
            cmd.ExecuteNonQuery()

            'set the output parameters
            ReDim _ReturnValue(0)
            If Not procParams Is Nothing Then
                For index As Integer = 0 To procParams.Length - 1
                    If cmd.Parameters(index).Direction = ParameterDirection.Output Or cmd.Parameters(index).Direction = ParameterDirection.InputOutput Then
                        ReDim Preserve _ReturnValue(UBound(_ReturnValue) + 1)
                        _ReturnValue(UBound(_ReturnValue)) = cmd.Parameters(index).Value
                    End If
                Next
            End If
            Return True
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        Finally
            If IsSingleConnection And Not cnn Is Nothing Then cnn.Close()
            If Not cmd Is Nothing Then cmd.Dispose()
        End Try
    End Function
    Public Function ExecuteCommandText(ByVal procName As String, ByVal flagP As Boolean, ByVal ParamArray procParams As IDbDataParameter()) As Boolean
        Dim cmd As SqlCommand = Nothing
        Try

            'prepare command
            cmd = New SqlCommand(procName)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 300
            If Not procParams Is Nothing Then
                For index As Integer = 0 To procParams.Length - 1
                    cmd.Parameters.Add(procParams(index))
                Next
            End If

            'set the connection
            If IsUsingTransaction Then
                cmd.Connection = Txn.Connection
                cmd.Transaction = Txn
            Else
                If IsSingleConnection Then
                    cnn = New SqlConnection(GetConnectionString)
                    cnn.Open()
                End If

                cmd.Connection = cnn
            End If

            'execute command
            cmd.ExecuteNonQuery()

            'set the output parameters
            ReDim _ReturnValue(0)
            If Not procParams Is Nothing Then
                For index As Integer = 0 To procParams.Length - 1
                    If cmd.Parameters(index).Direction = ParameterDirection.Output Or cmd.Parameters(index).Direction = ParameterDirection.InputOutput Then
                        ReDim Preserve _ReturnValue(UBound(_ReturnValue) + 1)
                        _ReturnValue(UBound(_ReturnValue)) = cmd.Parameters(index).Value
                    End If
                Next
            End If

            Return True
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        Finally
            If IsSingleConnection And Not cnn Is Nothing Then cnn.Close()
            If Not cmd Is Nothing Then cmd.Dispose()
        End Try
    End Function
    Public Function ExecuteCommandScalar(ByVal procName As String) As Object
        Dim cmd As SqlCommand = Nothing
        Try

            'prepare command
            cmd = New SqlCommand(procName)
            cmd.CommandType = CommandType.Text
            cmd.CommandTimeout = 300

            'set the connection
            If IsUsingTransaction Then
                cmd.Connection = Txn.Connection
                cmd.Transaction = Txn
            Else
                If IsSingleConnection Then
                    cnn = New SqlConnection(GetConnectionString)
                    cnn.Open()
                End If

                cmd.Connection = cnn
            End If

            'execute command
            Return cmd.ExecuteScalar()

        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        Finally
            If IsSingleConnection And Not cnn Is Nothing Then cnn.Close()
            If Not cmd Is Nothing Then cmd.Dispose()
        End Try
    End Function
    Public Function ExecuteDataTable(ByVal cmdtext As String, ByVal cmdtype As CommandType, ByVal ParamArray procParams As IDbDataParameter()) As DataTable
        Dim cmd As SqlCommand = Nothing
        Dim rdr As SqlDataReader = Nothing
        Dim da As SqlDataAdapter
        Dim ds As DataSet

        Try
            'prepare command
            cmd = New SqlCommand(cmdtext)
            cmd.CommandType = cmdtype
            cmd.CommandTimeout = 300

            If Not procParams Is Nothing Then
                For index As Integer = 0 To procParams.Length - 1
                    cmd.Parameters.Add(procParams(index))
                Next
            End If

            'execute data table
            If IsSingleConnection Then
                'set the connection
                crd = New SqlConnection(GetConnectionString)
                crd.Open()
                cmd.Connection = crd

                'execute datatable 
                'rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                da = New SqlDataAdapter(cmd)
            Else
                'set the connection
                cmd.Connection = cnn

                'execute datatable 
                'rdr = cmd.ExecuteReader()
                da = New SqlDataAdapter(cmd)
            End If

            ds = New DataSet
            da.Fill(ds)
            Return ds.Tables(0)

        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#Region "ExecuteDataReader Methods"
    Public Function ExecuteData() As String
        Dim cmd As SqlCommand = Nothing
        Dim rdr As SqlDataReader = Nothing
        Try
            crd = New SqlConnection(GetConnectionString)
            crd.Open()

            cmd = New SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 300
            cmd.Connection = crd

            Return cmd.ToString


            ''execute data reader
            'If IsSingleConnection Then
            '    'set the connection
            '    crd = New SqlConnection(GetConnectionString)
            '    crd.Open()
            '    cmd.Connection = crd

            'Else
            '    'set the connection
            '    cmd.Connection = cnn
            'End If

            'Return cmd.Connection.State.ToString

            'Catch sqlex As SqlException
            '    Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ExecuteDataReader(ByVal cmdtext As String, ByVal cmdtype As CommandType, ByVal ParamArray procParams As IDbDataParameter()) As SqlDataReader
        Dim cmd As SqlCommand = Nothing
        Dim rdr As SqlDataReader = Nothing
        Try
            'prepare command
            cmd = New SqlCommand(cmdtext)
            cmd.CommandType = cmdtype
            cmd.CommandTimeout = 300

            If Not procParams Is Nothing Then
                For index As Integer = 0 To procParams.Length - 1
                    cmd.Parameters.Add(procParams(index))
                Next
            End If

            'execute data reader
            If IsSingleConnection Then
                'set the connection
                crd = New SqlConnection(GetConnectionString)
                crd.Open()
                cmd.Connection = crd

                'execute reader 
                rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            Else
                'set the connection
                cmd.Connection = cnn

                'execute reader 
                rdr = cmd.ExecuteReader()
            End If

            Return rdr
        Catch sqlex As SqlException
            Throw NewException(sqlex)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region

#Region "Try Connection"
    Public Function CanConnect(ByVal pConnectionString As String) As Boolean
        Dim cnn As SqlConnection = Nothing
        Dim bconnect As Boolean = False
        Try
            cnn = New SqlConnection(pConnectionString)
            cnn.Open()
            bconnect = True
        Catch ex As Exception
            bconnect = False
        Finally
            cnn.Close()
            cnn.Dispose()
        End Try
        Return bconnect
    End Function
#End Region

#End Region

#Region "Exceptions "
    Private Function NewException(ByVal sqlex As SqlException) As Exception
        Return New Exception(sqlex.Message + vbCrLf + "detail: " + sqlex.Procedure + "-" + sqlex.LineNumber.ToString)
    End Function
#End Region

End Class

